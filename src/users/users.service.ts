import {
  BadRequestException,
  ClassSerializerInterceptor,
  ConflictException,
  Injectable,
  UseInterceptors,
} from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import * as bcrypt from 'bcrypt';
import { Prisma, User } from '@prisma/client';
import { CreateUserDto } from './dto/create-user.dto';
import { PasswordService } from './password.service';

@Injectable()
export class UsersService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly passwordService: PasswordService,
  ) {}

  async findOne(
    userWhereUniqueInput: Prisma.UserWhereUniqueInput,
  ): Promise<User | null> {
    return this.prisma.user.findUnique({
      where: userWhereUniqueInput,
    });
  }

  async findAll(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.UserWhereUniqueInput;
    where?: Prisma.UserWhereInput;
    orderBy?: Prisma.UserOrderByWithRelationInput;
  }): Promise<User[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.user.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  // async create_(data: Prisma.UserCreateInput): Promise<User> {
  //   const user = await this.prisma.user.create({
  //     data,
  //   });

  //   user.password = undefined;

  //   return user;
  // }

  async update(params: {
    where: Prisma.UserWhereUniqueInput;
    data: Prisma.UserUpdateInput;
  }): Promise<User> {
    const { where, data } = params;
    return this.prisma.user.update({
      data,
      where,
    });
  }

  async remove(where: Prisma.UserWhereUniqueInput): Promise<User> {
    return this.prisma.user.delete({
      where,
    });
  }

  // use by auth module to register user in database
  @UseInterceptors(ClassSerializerInterceptor)
  async create(userDto: CreateUserDto): Promise<User> {
    const userExist = await this.prisma.user.findFirst({
      where: {
        OR: [
          { name: userDto.name },
          { name: userDto.email },
          { email: userDto.name },
          { email: userDto.email },
        ],
      },
    });

    if (userExist) {
      throw new ConflictException('user_already_exist');
    }

    if (userDto.password.length < 6) {
      throw new BadRequestException('password_to_short');
    }

    const password = await this.passwordService.hashPassword(userDto.password);

    const user = await this.prisma.user.create({
      data: <Prisma.UserCreateInput>{
        ...userDto,
        password,
        activatedAt: new Date(),
      },
    });

    user.password = undefined;

    return user;
  }

  //use by auth module to login user
  async findByLogin(login: string): Promise<User> {
    const user = this.prisma.user.findFirst({
      where: {
        AND: [
          { deletedAt: null, NOT: { activatedAt: null } },
          { OR: [{ email: login }, { name: login }] },
        ],
      },
    });

    return user;
  }

  // see https://medium.com/@bruceguenkam/how-to-create-authentication-system-with-jwt-using-nestjs-and-prisma-e803d899a7a7

  // use by user module to change user password
  // @UseInterceptors(ClassSerializerInterceptor)
  // async updatePassword(id: string, payload: UpdatePasswordDto): Promise<User> {
  //   const user = await this.prisma.user.findUnique({
  //     where: { id },
  //   });

  //   // check old passwords
  //   const validateUser =
  //     user &&
  //     (await this.authService.validateUser(user.name, payload.password));

  //   if (!validateUser) {
  //     throw new UnauthorizedException('Invalid credentials');
  //   }

  //   const password = await userPassword.hashAsync(payload.newPassword);

  //   return this.prisma.user.update({
  //     where: { id },
  //     data: { password },
  //   });
  // }
}

// export const userPassword = {
//   hashAsync: async (pwd: string): Promise<string> => {
//     const salt = bcrypt.genSaltSync(10);
//     const hash = bcrypt.hash(pwd, salt);

//     return hash;
//   },

//   compareAsync: async (pwd: string, hash: string): Promise<Boolean> =>
//     (pwd && hash && bcrypt.compare(pwd, hash)) || false,
// };
