import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty, Length, Validate } from 'class-validator';
import { UserEntity } from '../entities/user.entity';

export class CreateUserDto extends PartialType(UserEntity) {
  @ApiProperty({ type: String, required: true })
  name: string;

  @ApiProperty({ type: String, required: true })
  email: string;

  @ApiProperty({ type: String, required: true })
  @IsNotEmpty()
  @Length(6, 64)
  @Validate((s) => {
    return !!s;
  })
  password: string;
}
