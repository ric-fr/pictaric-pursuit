import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from '../entities/user.entity';

export class CreateUserDto {
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;

  // @IsNotEmpty()
  // @ApiProperty()
  // login: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;
}

// export class UpdatePasswordDto {
//   @IsNotEmpty()
//   @ApiProperty({ description: `Current user password` })
//   password: string;

//   @IsNotEmpty()
//   @ApiProperty()
//   newPassword: string;
// }

// export interface RegistrationStatus {
//   success: boolean;
//   message: string;
//   data?: UserEntity;
// }

// export interface RegistrationSeederStatus {
//   success: boolean;
//   message: string;
//   data?: UserEntity[];
// }
