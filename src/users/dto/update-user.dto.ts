import { PartialType } from '@nestjs/swagger';
import { UserEntity } from '../entities/user.entity';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(UserEntity) {}
