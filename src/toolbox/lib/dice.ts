import { randomInt } from 'crypto';
import { DiceParamsDto, DiceScoreTypeEnum, DiceRollDto } from '../dto/dice.dto';

class Dice {
  private params: DiceParamsDto = {
    nb: 1,
    facet: 6,
    scoreType: DiceScoreTypeEnum.sum,
  };

  constructor(args: DiceParamsDto = {}) {
    this.configure(args);
  }

  public configure(args: DiceParamsDto): void {
    Object.assign(this.params, args);
  }

  public async roll(nb = undefined): Promise<DiceRollDto> {
    const dices: number[] = [];
    nb = nb || this.params.nb;

    for (let i = 0; i < nb; i++) {
      const dice = randomInt(1, this.params.facet + 1);
      dices.push(dice);
    }

    return this.dicesScore(dices);
  }

  public async dicesScore(dices: number[]): Promise<DiceRollDto> {
    const countFacets: number[] = Array(this.params.facet).fill(0);
    const sequences: number[][] = [];
    const sum: number = dices.reduce((partialSum, a) => partialSum + a, 0);
    let score: number;

    /**
     * Count dices by faces (aka. pairs)
     */
    for (let i = 0; i < this.params.facet; i++) {
      countFacets[i] = dices.filter((dice) => dice === i + 1).length;
    }

    /**
     * For each facet with count != 0 try to find sequences
     */
    for (let i: number = 0; i < this.params.facet - 1; i) {
      if (countFacets[i] && countFacets[i + 1]) {
        for (
          let j: number = i + 1;
          dices.includes(j + 1) && j < this.params.facet;
          j++
        ) {
          Array.isArray(sequences[i]) || (sequences[i] = [i + 1]);
          sequences[i].push(j + 1);
        }
      }

      i += (sequences[i]?.length || 0) + 1;
    }

    switch (this.params.scoreType) {
      case DiceScoreTypeEnum.sum:
        score = sum;
        break;
      default:
        throw new Error(
          `scoreType '${this.params.scoreType}' isn't implemented.`,
        );
    }

    const result: DiceRollDto = {
      score,
      sum,
      dices: dices,
      countFacets,
      sequences: sequences.filter((i) => i), // filter out nulls

      scoreType: this.params.scoreType,
      facet: this.params.facet,
    };

    return result;
  }
}

export default Dice;
