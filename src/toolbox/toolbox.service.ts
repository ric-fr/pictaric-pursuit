import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import Dice from 'src/toolbox/lib/dice';
import { DiceParamsDto, DiceRollDto } from 'src/toolbox/dto/dice.dto';

@Injectable()
export class ToolboxService {
  constructor(private readonly prisma: PrismaService) {}

  diceRoll(params: DiceParamsDto = {}): Promise<DiceRollDto> {
    return new Dice(params).roll();
  }
}
