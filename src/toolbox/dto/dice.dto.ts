import { ApiPropertyOptional, ApiProperty } from '@nestjs/swagger';
import { IsOptional, Min, Max, IsInt, IsEnum } from 'class-validator';

export enum DiceScoreTypeEnum {
  sum = 'sum',
}

export class DiceParamsDto {
  @ApiPropertyOptional({ type: 'integer', minimum: 1, maximum: 20, default: 1 })
  @IsOptional()
  @Min(1)
  @Max(20)
  @IsInt()
  nb?: number;

  @ApiPropertyOptional({ type: 'integer', minimum: 2, maximum: 20, default: 6 })
  @IsOptional()
  @Min(2)
  @Max(20)
  @IsInt()
  facet?: number;

  @ApiPropertyOptional({
    type: 'enum',
    enum: DiceScoreTypeEnum,
    default: DiceScoreTypeEnum.sum,
  })
  @IsOptional()
  @IsEnum(DiceScoreTypeEnum)
  scoreType?: DiceScoreTypeEnum = DiceScoreTypeEnum.sum;
}

export class DiceDicesDto {
  @ApiProperty({
    type: 'integer',
    minimum: 1,
    isArray: true,
    example: [1, 2, 1, 4, 5],
  })
  dices: number[];
}

export class DiceRollDto extends DiceDicesDto {
  @ApiProperty({ type: Number, example: 9 })
  score: number;

  @ApiProperty({ type: Number, example: 9 })
  sum: number;

  @ApiProperty({ type: Number, example: [2, 1, 0, 1, 1], isArray: true })
  countFacets: number[];

  @ApiProperty({
    type: [Number],
    example: [
      [1, 2],
      [4, 5],
    ],
    isArray: true,
  })
  sequences: number[][];

  @ApiProperty({ type: Number, example: 6 })
  facet: number;

  @ApiProperty({
    type: 'enum',
    enum: DiceScoreTypeEnum,
    example: DiceScoreTypeEnum.sum,
  })
  @IsEnum(DiceScoreTypeEnum)
  scoreType?: DiceScoreTypeEnum;
}
