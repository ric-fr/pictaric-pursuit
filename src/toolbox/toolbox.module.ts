import { Module } from '@nestjs/common';
import { ToolboxService } from './toolbox.service';

@Module({
  providers: [ToolboxService],
  exports: [ToolboxService],
})
export class ToolboxModule {}
