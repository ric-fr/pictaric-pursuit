import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  ParseUUIDPipe,
  UseGuards,
  Req,
  ForbiddenException,
  Query,
  Header,
} from '@nestjs/common';
import { PlayersService } from './players.service';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { JwtPlayerGuard } from './jwt-player/jwt-player.guard';
import { ReqPlayerDto } from './jwt-player/req-player.dto';
import {
  PlayerAcceptBodyDto,
  PlayerAcceptResponseDto,
} from './dto/player-accept.dto';
import { AuthGuard } from '@nestjs/passport';
import { DiceRollDto, DiceParamsDto } from 'src/toolbox/dto/dice.dto';
import { ToolboxService } from 'src/toolbox/toolbox.service';

@Controller('players')
export class PlayersController {
  constructor(
    private readonly service: PlayersService,
    private readonly toolboxService: ToolboxService,
  ) {}

  @Post('me')
  @ApiBearerAuth('player-token')
  @UseGuards(JwtPlayerGuard)
  @ApiResponse({
    description: `Returns req.player (mainly for debug purpose).`,
    type: ReqPlayerDto,
  })
  me(@Req() req) {
    return req.player;
  }

  @UseGuards(AuthGuard(['jwt-player'])) // this way we can combine OR Guards
  @ApiBearerAuth('player-token')
  @UseGuards(JwtPlayerGuard)
  @Post('dice')
  @ApiResponse({ type: DiceRollDto })
  async dice(@Body() params: DiceParamsDto): Promise<DiceRollDto> {
    return this.toolboxService.diceRoll(params);
  }

  @UseGuards(AuthGuard(['jwt-player']))
  @ApiBearerAuth('player-token')
  @Get('dice')
  @Header('Cache-Control', 'no-cache,no-store,must-revalidate')
  @ApiResponse({ type: DiceRollDto })
  async diceGet(@Query() params: DiceParamsDto): Promise<DiceRollDto> {
    return this.toolboxService.diceRoll(params);
  }

  @Post('accept')
  @ApiBearerAuth('player-token')
  @UseGuards(JwtPlayerGuard)
  @ApiResponse({ type: PlayerAcceptResponseDto })
  async acceptPlayer(
    @Req() req,
    @Body() body: PlayerAcceptBodyDto, // for swagger
  ): Promise<PlayerAcceptResponseDto> {
    const gameId = req.player.gameId;
    const playerId = req.body.playerId;

    const reqPlayerDto: ReqPlayerDto = req.player;

    // check if requesting player is accepted in this game
    const reqPlayer = await this.service.findById(reqPlayerDto.id);

    // only accepted players can accept new ones
    if (reqPlayer && reqPlayer.gameId === gameId && reqPlayer.acceptedAt) {
      const result = this.service.accept(gameId, playerId);

      return result;
    }

    throw new ForbiddenException();
  }

  @Get(':id')
  findOne(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.service.findById(id);
  }

  // @Patch(':id')
  // update(
  //   @Param('id', new ParseUUIDPipe()) id: string,
  //   @Body() updatePlayerDto: UpdatePlayerDto,
  // ) {
  //   return this.service.update(id, updatePlayerDto);
  // }

  // @Delete(':id')
  // remove(@Param('id', new ParseUUIDPipe()) id: string) {
  //   return this.service.remove(id);
  // }
}
