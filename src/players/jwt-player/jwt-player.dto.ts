import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, IsString } from 'class-validator';

export class JwtPlayerDto {
  @ApiProperty({ type: String, format: 'uuid' })
  @IsUUID(4)
  sub: string;

  @ApiProperty({ type: String })
  @IsString()
  name: string;

  @ApiProperty({ type: String, format: 'uuid' })
  @IsUUID(4)
  gameId: string;
}
