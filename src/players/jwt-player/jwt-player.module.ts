import { forwardRef, Module } from '@nestjs/common';
import { JwtModule, JwtModuleOptions, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { PlayersModule } from '../players.module';
import { JwtPlayerGuard } from './jwt-player.guard';
import { JwtPlayerStrategy } from './jwt-player.strategy';

export const jwtPlayerOptions: JwtModuleOptions = {
  secret: process.env.JWT_PLAYER_SECRET || 'secret',
  signOptions: {
    expiresIn: process.env.JWT_PLAYER_EXPIRES_IN || undefined,
  },
  verifyOptions: {
    ignoreExpiration: true,
  },
};

@Module({
  imports: [
    PassportModule.register({
      defaultStrategy: 'jwt-player',
      property: 'player',
      session: false,
    }),
    JwtModule.register(jwtPlayerOptions),
    forwardRef(() => PlayersModule),
  ],
  providers: [
    {
      provide: 'JwtPlayerService',
      useExisting: JwtService,
    },
    JwtPlayerGuard,
    JwtPlayerStrategy,
  ],
  exports: ['JwtPlayerService', PassportModule],
})
export class JwtPlayerModule {}
