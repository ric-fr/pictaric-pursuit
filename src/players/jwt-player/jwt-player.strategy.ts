import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { JwtPlayerDto } from './jwt-player.dto';
import { ReqPlayerDto } from './req-player.dto';
import { PlayersService } from '../players.service';

@Injectable()
export class JwtPlayerStrategy extends PassportStrategy(
  Strategy,
  'jwt-player',
) {
  constructor(private readonly service: PlayersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: process.env.JWT_PLAYER_SECRET || 'secret',
    });
  }

  async validate(payload: JwtPlayerDto): Promise<ReqPlayerDto> {
    const player = await this.service.findById(payload.sub);

    if (player) {
      const { id, name, gameId } = player;

      const reqPlayer: ReqPlayerDto = {
        id,
        name,
        gameId,
      };

      return reqPlayer;
    }
  }
}
