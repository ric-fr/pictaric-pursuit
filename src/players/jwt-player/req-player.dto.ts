import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, IsString } from 'class-validator';
import { Player } from 'prisma/shared/classes/player';

export class ReqPlayerDto {
  @ApiProperty({ type: String, format: 'uuid' })
  @IsUUID(4)
  id: string;

  @ApiProperty({ type: String })
  @IsString()
  name: string;

  @ApiProperty({ type: String, format: 'uuid' })
  @IsUUID(4)
  gameId: string;
}
