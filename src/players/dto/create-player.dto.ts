export class CreatePlayerDto {
  name: string;
  gameId: string;
  accepted?: Boolean;
}
