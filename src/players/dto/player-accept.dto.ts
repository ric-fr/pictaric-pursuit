import { ApiProperty } from '@nestjs/swagger';

export class PlayerAcceptBodyDto {
  @ApiProperty({
    type: String,
    format: 'uuid',
  })
  playerId: string;
}

export class PlayerAcceptResponseDto extends PlayerAcceptBodyDto {
  @ApiProperty({
    type: String,
    format: 'uuid',
  })
  gameId: string;

  @ApiProperty({ type: Date, required: false })
  acceptedAt: Date | null;
}
