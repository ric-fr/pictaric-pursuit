import { Injectable, NotAcceptableException } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { CreatePlayerDto } from './dto/create-player.dto';
import { UpdatePlayerDto } from './dto/update-player.dto';
import { Player, Prisma } from '@prisma/client';
import { PlayerAcceptResponseDto } from './dto/player-accept.dto';
import { PrismaServiceOrTransaction } from 'src/shared/utils/prisma-service-or-transaction.type';

@Injectable()
export class PlayersService {
  constructor(private readonly prisma: PrismaService) {}

  create(
    createPlayerDto: CreatePlayerDto,
    {
      accepted = false,
      prisma = this.prisma,
      select = {
        id: true,
        name: true,
        createdAt: true,
        updatedAt: true,
        acceptedAt: true,
        gameId: true,
        game: {
          select: {
            id: true,
            name: true,
            private: true,
            players: {
              select: {
                name: true,
                createdAt: true,
                updatedAt: true,
                acceptedAt: true,
              },
            },
            board: true,
            cardSet: true,
            createdAt: true,
            updatedAt: true,
          },
        },
      },
    }: {
      accepted?: boolean;
      prisma?: PrismaServiceOrTransaction;
      select?: Prisma.PlayerSelect;
    } = {},
  ): Promise<Player> {
    const { name, gameId } = createPlayerDto;

    const playerCreateArgs: Prisma.PlayerCreateArgs = {
      data: {
        name,
        gameId,
        acceptedAt: accepted ? new Date() : null,
      },
      select,
    };

    return prisma.player.create(playerCreateArgs);
  }

  /**
   *
   * @param gameId   uuid
   * @param playerId uuid
   * @returns Promise<PlayerAcceptResponseDto>
   */

  async accept(
    gameId: string,
    playerId: string,
    {
      prisma = this.prisma,
    }: {
      prisma?: PrismaServiceOrTransaction;
    } = {},
  ): Promise<PlayerAcceptResponseDto> {
    const accept = await prisma.player.updateMany({
      where: {
        id: playerId,
        gameId,
        acceptedAt: null, // only update not accepted yet
      },
      data: {
        acceptedAt: new Date(),
      },
    });

    const player = await prisma.player.findFirstOrThrow({
      where: { id: playerId, gameId },
    });

    const response: PlayerAcceptResponseDto = {
      playerId: player.id,
      gameId: player.gameId,
      acceptedAt: player.acceptedAt,
    };

    return response;
  }

  findById(
    id: string,
    {
      prisma = this.prisma,
    }: {
      prisma?: PrismaServiceOrTransaction;
    } = {},
  ): Promise<Player> {
    return prisma.player.findUniqueOrThrow({ where: { id } });
  }
}
