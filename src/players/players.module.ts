import { Module } from '@nestjs/common';
import { PlayersService } from './players.service';
import { PlayersController } from './players.controller';
import { JwtPlayerModule } from './jwt-player/jwt-player.module';
import { ToolboxModule } from 'src/toolbox/toolbox.module';

@Module({
  imports: [JwtPlayerModule, ToolboxModule],
  controllers: [PlayersController],
  providers: [PlayersService],
  exports: [PlayersService, JwtPlayerModule],
})
export class PlayersModule {}
