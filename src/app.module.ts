import { Logger, Module } from '@nestjs/common';
import {
  JwtModule,
  JwtSecretRequestType,
  JwtSignOptions,
  JwtVerifyOptions,
} from '@nestjs/jwt';
import { loggingMiddleware, PrismaModule } from 'nestjs-prisma';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { GamesModule } from './games/games.module';
// import { loggingMiddleware } from './shared/prisma-logging.middleware';
import { UsersModule } from './users/users.module';
import { PlayersModule } from './players/players.module';
import { ToolboxModule } from './toolbox/toolbox.module';

@Module({
  imports: [
    // ConfigModule.forRoot({ isGlobal: true, load: [config] }),
    PrismaModule.forRoot({
      isGlobal: true,
      prismaServiceOptions: {
        prismaOptions: {
          log: ['info', 'query', 'error', 'warn'],
        },
        middlewares: [
          loggingMiddleware({
            logger: new Logger('PrismaMiddleware'),
            logLevel: 'log',
          }),
        ],
        // middlewares: [loggingMiddleware(new Logger('PrismaMiddleware'))], // configure your prisma middleware
      },
    }),
    AuthModule,
    UsersModule,
    GamesModule,
    PlayersModule,
    ToolboxModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
