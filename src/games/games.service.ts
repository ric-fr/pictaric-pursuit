import {
  ConflictException,
  Inject,
  Injectable,
  NotFoundException,
  Req,
} from '@nestjs/common';
import { CreateGameDto } from './dto/create-game.dto';
import { UpdateGameDto } from './dto/update-game.dto';
import { Game, Prisma } from '@prisma/client';
import { GameEntity } from './entities/game.entity';
import { JoinGameDto } from './dto/join-game.dto';
import { Player } from 'prisma/shared/classes/player';
import { DeepPartial } from 'src/shared/utils/deep-partial.type';
import { PrismaService } from 'nestjs-prisma';
import { PlayersService } from 'src/players/players.service';
import { CreatePlayerDto } from 'src/players/dto/create-player.dto';
import { JwtPlayerDto } from 'src/players/jwt-player/jwt-player.dto';
import { JwtService } from '@nestjs/jwt';
import { ApiProperty } from '@nestjs/swagger';
import { PrismaServiceOrTransaction } from 'src/shared/utils/prisma-service-or-transaction.type';
import { Request } from 'express';

export class JoinGameResponse {
  token: string;
  player: DeepPartial<Player>;
}

export type StatedEndedAt = {
  startedAt: Date | null;
  endedAt: Date | null;
};

export enum GameStatusEnum {
  notStarted = 'NOT_STARTED',
  started = 'STARTED',
  ended = 'ENDED',
}

// Extend the T generic with the status attribute
export type WithStatus<T> = T & {
  status: GameStatusEnum;
};

export class GameWithStatus
  extends GameEntity
  implements WithStatus<GameEntity>
{
  @ApiProperty({ type: 'enum', enum: GameStatusEnum })
  status: GameStatusEnum;
}

@Injectable()
export class GamesService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly playersService: PlayersService,
    @Inject('JwtPlayerService') private readonly jwtPlayerService: JwtService,
  ) {}

  // Take objects that satisfy FirstLastName and computes a full name
  private computeStatus<T extends StatedEndedAt>(game: T): WithStatus<T> {
    let status = GameStatusEnum.notStarted;

    if (game.startedAt) {
      if (game.endedAt) {
        status = GameStatusEnum.ended;
      } else {
        status = GameStatusEnum.started;
      }
    } else {
      status = GameStatusEnum.notStarted;
    }

    return {
      ...game,
      status,
    };
  }

  async create(createGameDto: CreateGameDto): Promise<JoinGameResponse> {
    const { name, boardNodeLabel, cardSetNodeLabel, playerName } =
      createGameDto;

    let createArgs: Prisma.GameCreateArgs = {
      data: {
        name,
        boardNodeLabel,
        cardSetNodeLabel,
      },
    };

    return this.prisma.$transaction(async (prisma) => {
      const game = await prisma.game.create(createArgs);

      return this.join(
        { gameId: game.id, name: playerName },
        { accepted: true, prisma },
      );
    });
  }

  /**
   *
   * @param joinGameDto
   * @param args { accepted: boolean, prisma }
   * @returns
   */
  async join(
    joinGameDto: JoinGameDto,
    {
      accepted = false,
      prisma = this.prisma,
    }: {
      accepted?: boolean;
      prisma?: PrismaServiceOrTransaction;
    } = {},
  ): Promise<JoinGameResponse> {
    const { name, gameId } = joinGameDto;

    const createPlayerDto: CreatePlayerDto = {
      name,
      gameId,
    };

    try {
      const player = await this.playersService.create(createPlayerDto, {
        accepted,
        prisma,
      });

      const payload: JwtPlayerDto = {
        sub: player.id,
        name: player.name,
        gameId: player.gameId,
      };
      const token = this.jwtPlayerService.sign(payload);

      return {
        token,
        player,
      };
    } catch (error) {
      if (!error.message) {
        if (error.code == 'P2002') {
          error.message = `The name "${name}" is already used by an other player.`;
        }
      }
      throw new ConflictException(error);
    }
  }

  async start(
    id: string,
    // {
    //   prisma = this.prisma,
    // }: {
    //   prisma?: PrismaServiceOrTransaction;
    // } = {},
  ): Promise<Game> {
    // const game = await prisma.game.findFirst({
    //   where: { id, startedAt: null },
    // });

    // if (!game) {
    //   throw new NotFoundException(
    //     `Failed to find game to start. Is the game already started?`,
    //   );
    // }

    const game = await this.prisma.game.findFirstOrThrow({
      where: { id, startedAt: null },
    });

    // game is already started
    // if (game.startedAt !== null) {
    //   return game;
    // }

    const startedAt = new Date();

    try {
      return await this.prisma.$transaction(
        async (prisma: PrismaServiceOrTransaction) => {
          /**
           * Wrapping in a transaction so if an error ocurred startedAt would stay null
           * using SQLite I'm guessing writes lock
           */

          const updateGame = await prisma.game.updateMany({
            where: {
              id,
              startedAt: null,
            },
            data: {
              startedAt,
              lock: {
                increment: 1,
              },
            },
          });

          if (updateGame.count !== 1) {
            throw new Error(`Could not start the game.`);
          }

          const game = await this.prisma.game.findFirst({
            where: { id, lock: 1 },
          });

          if (!game) {
            throw new Error(
              `Could not start the game. (Lock indicates other player started it)`,
            );
          }

          /**
           * We can now safely initialise the game
           * As any error now on would ROLLBACK the TRANSACTION
           */

          // TODO: Emit socketIo 'Game started'

          return game;
        },
      );
    } catch (error) {
      console.error(`Error: starting game ${id}`, error);

      return this.prisma.game.findUniqueOrThrow({
        where: { id },
      });
    }
  }

  async findAll(): Promise<Partial<WithStatus<Game>>[]> {
    const games = await this.prisma.game.findMany({
      where: {
        private: false,
      },
      select: {
        id: true,
        name: true,
        createdAt: true,
        updatedAt: true,
        startedAt: true,
        endedAt: true,
        boardNodeLabel: true,
        cardSetNodeLabel: true,
        players: {
          select: {
            name: true,
            createdAt: true,
            updatedAt: true,
            acceptedAt: true,
            playerActions: {
              orderBy: {
                createdAt: 'desc',
              },
              where: {
                NOT: {
                  moveToNodeId: null,
                },
              },
              take: 1,
            },
          },
          orderBy: {
            createdAt: 'asc',
          },
        },
      },
      orderBy: {
        createdAt: 'desc',
      },
    });

    const gamesWithStatus: Partial<WithStatus<Game>>[] = games.map((game) =>
      this.computeStatus(game),
    );

    return gamesWithStatus;
  }

  async findOne(id: string) {
    const game = await this.prisma.game.findFirstOrThrow({
      where: {
        id,
        private: false,
        secret: null,
      },
      select: {
        id: true,
        name: true,
        boardNodeLabel: true,
        cardSetNodeLabel: true,
        createdAt: true,
        updatedAt: true,
        startedAt: true,
        endedAt: true,
        private: true,
        board: true,
        cardSet: true,
        players: {
          select: {
            name: true,
            createdAt: true,
            updatedAt: true,
            acceptedAt: true,
            playerActions: {
              where: {
                moveToNodeId: {
                  not: {
                    equals: null,
                  },
                },
              },
              orderBy: {
                createdAt: 'desc',
              },
              take: 1,
            },
          },
          orderBy: {
            createdAt: 'asc',
          },
        },
      },
    });

    // the easy way was orderBy: unknown
    const orderBy: Prisma.Enumerable<Prisma.NodeOrderByWithRelationInput> = {
      label: <Prisma.SortOrder>'asc',
    };

    const whereBoardLabel = {
      label: {
        startsWith: `${game.board.label}.`,
      },
    };

    const boardNodes = await this.prisma.node.findMany({
      where: whereBoardLabel,
      orderBy,
      // include: {
      //   ancestorEdges: true,
      // },
    });

    const boardEdges = await this.prisma.edge.findMany({
      where: {
        OR: [
          {
            ancestorNode: whereBoardLabel,
          },
          {
            descendantNode: whereBoardLabel,
          },
        ],
      },
    });

    const cardDecks = await this.prisma.node.findMany({
      where: {
        type: 'cardDeck',
        label: {
          startsWith: `${game.cardSet.label}.cardDeck.`,
        },
      },
      orderBy,
    });

    // const {secret, ...gameSafe} = this.computeStatus(game)

    const response = {
      game: this.computeStatus(game),
      boardNodes,
      boardEdges,
      cardDecks,
    };

    return response;
  }

  update(id: string, updateGameDto: UpdateGameDto) {
    return `This action updates a #${id} game`;
  }

  remove(id: string) {
    return `This action removes a #${id} game`;
  }
}
