import { Module } from '@nestjs/common';
import { GamesService } from './games.service';
import { GamesController } from './games.controller';
import { PlayersModule } from 'src/players/players.module';
import { ToolboxModule } from 'src/toolbox/toolbox.module';

@Module({
  imports: [PlayersModule, ToolboxModule],
  providers: [GamesService],
  exports: [GamesService],
  controllers: [GamesController],
})
export class GamesModule {}
