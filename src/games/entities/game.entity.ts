import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Game as GameClass } from 'prisma/shared/classes/game';
import { Player } from 'prisma/shared/classes/player';
import { Node } from 'prisma/shared/classes/node';
import { Exclude } from 'class-transformer';
import { GameStatusEnum } from '../games.service';

export class GameEntity {
  constructor(partial: Partial<GameEntity>) {
    Object.assign(this, partial);
  }

  @ApiProperty({ type: String, format: 'uuid' })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: Date })
  startedAt: Date;

  @ApiProperty({ type: Date })
  endedAt: Date;

  @ApiProperty({ type: Boolean, default: false })
  private: boolean = false;

  @ApiProperty({ type: String })
  name: string;

  @ApiPropertyOptional({ type: String })
  @Exclude()
  secret?: string;

  @ApiProperty({ type: String })
  boardNodeLabel: string;

  @ApiPropertyOptional({ type: () => Node })
  board?: Node;

  @ApiProperty({ type: String })
  cardSetNodeLabel: string;

  @ApiPropertyOptional({ type: () => Node })
  cardSet?: Node;

  @ApiPropertyOptional({ isArray: true, type: () => Player })
  players?: Player[];
}
