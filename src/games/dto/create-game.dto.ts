import { ApiProperty, ApiTags, PartialType } from '@nestjs/swagger';
import { IsUUID, Matches, ValidateIf } from 'class-validator';
import { Game } from 'prisma/shared/validators/models/Game.model';

export class CreateGameDto {
  @ApiProperty({
    example: `MyNewGame`,
    description: `Creates a new game`,
  })
  name: string;

  @ApiProperty({
    example: `Julian Assange`,
    description: `Player nickname`,
  })
  playerName: string;

  @ApiProperty({
    required: false,
    default: false,
  })
  private?: boolean;

  @ApiProperty({
    required: false,
    default: null,
  })
  secret?: string;

  @ApiProperty({
    required: false,
    default: 'top.board.default',
  })
  @ValidateIf((object, value) => value !== undefined) // bypass validation
  @Matches(/^top\.board\.[a-z\-]+$/, {
    message: `Invalid bord label`,
  })
  boardNodeLabel?: string;

  @ApiProperty({
    required: false,
    default: 'top.cardSet.default',
  })
  @ValidateIf((object, value) => value !== undefined) // bypass validation
  @Matches(/^top\.cardSet\.[a-z\-]+$/, {
    message: `Invalid cardSet label`,
  })
  cardSetNodeLabel?: string;
}
