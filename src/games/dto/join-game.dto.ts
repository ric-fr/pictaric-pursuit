import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';

export class JoinGameDto {
  @ApiProperty({
    example: 'Nickname',
  })
  name: string;

  @ApiProperty({
    format: 'uuid',
  })
  @IsUUID()
  gameId: string;
}
