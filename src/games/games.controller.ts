import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ClassSerializerInterceptor,
  UseInterceptors,
  Req,
  ForbiddenException,
  UseGuards,
  Header,
  Query,
} from '@nestjs/common';
import {
  GamesService,
  GameWithStatus,
  JoinGameResponse,
} from './games.service';
import { CreateGameDto } from './dto/create-game.dto';
import { UpdateGameDto } from './dto/update-game.dto';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiResponse,
} from '@nestjs/swagger';
import { Game } from '@prisma/client';
import { GameEntity } from './entities/game.entity';
import { JoinGameDto } from './dto/join-game.dto';
import { Player } from 'prisma/shared/classes/player';
import { JwtPlayerGuard } from 'src/players/jwt-player/jwt-player.guard';
import { ReqPlayerDto } from 'src/players/jwt-player/req-player.dto';
import { PlayersService } from 'src/players/players.service';
import { ToolboxService } from 'src/toolbox/toolbox.service';
import { DiceRollDto, DiceParamsDto } from 'src/toolbox/dto/dice.dto';

@Controller('games')
export class GamesController {
  constructor(
    private readonly service: GamesService,
    private readonly playersService: PlayersService,
    private readonly toolboxService: ToolboxService,
  ) {}

  @Post('new')
  @UseInterceptors(ClassSerializerInterceptor)
  // @ApiDefaultResponse({
  //   status: 201,
  //   description: `Created a new game`,
  // })
  @ApiResponse({ type: GameEntity })
  async create(
    @Body() createGameDto: CreateGameDto,
  ): Promise<JoinGameResponse> {
    const join = await this.service.create(createGameDto);
    // const gameEntity = new GameWithStatus(game); // to activate ClassSerializerInterceptor and hide secret (see GameEntity constructor)
    // return gameEntity;

    return join;
  }

  @Post('join')
  @ApiCreatedResponse({
    type: Player,
  })
  join(@Body() joinGameDto: JoinGameDto): Promise<JoinGameResponse> {
    return this.service.join(joinGameDto);
  }

  @Post('start')
  @ApiBearerAuth('player-token')
  @UseGuards(JwtPlayerGuard)
  async start(@Req() req): Promise<Game> {
    const reqPlayerDto: ReqPlayerDto = req.player;

    const player = await this.playersService.findById(reqPlayerDto.id);

    if (player && player.acceptedAt) {
      const startedGame = await this.service.start(reqPlayerDto.gameId);

      return startedGame;
    }

    throw new ForbiddenException();
  }

  @Post('dice')
  @ApiResponse({ type: DiceRollDto })
  async dice(@Body() params: DiceParamsDto): Promise<DiceRollDto> {
    return this.toolboxService.diceRoll(params);
  }

  @Get('dice')
  @Header('Cache-Control', 'no-cache,no-store,must-revalidate')
  @ApiResponse({ type: DiceRollDto })
  async diceGet(@Query() params: DiceParamsDto): Promise<DiceRollDto> {
    return this.toolboxService.diceRoll(params);
  }

  @Get()
  @ApiResponse({ type: GameWithStatus, isArray: true })
  async findAll(): Promise<GameWithStatus[]> {
    const games = await this.service.findAll();

    return games.map((game) => new GameWithStatus(game));
  }

  @Get(':id')
  @ApiResponse({ type: GameWithStatus })
  findOne(@Param('id') id: string) {
    return this.service.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateGameDto: UpdateGameDto) {
    return this.service.update(id, updateGameDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
