import {
  Body,
  Controller,
  Post,
  UseGuards,
  UseInterceptors,
  Request,
  ClassSerializerInterceptor,
  Put,
  BadRequestException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/user.dto';
import { ApiBearerAuth, ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { LocalAuthGuard } from './guards/local-auth.guards';
import { User } from '@prisma/client';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { UsersService } from 'src/users/users.service';
import { AuthLoginDto } from './dto/auth-login.dto';
import { AuthUpdatePasswordDto } from './dto/auth-update-password.dto';
import { AuthLoginResponseDto } from './dto/auth-login-response.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Post('register')
  @UseInterceptors(ClassSerializerInterceptor)
  public async register(@Body() createUserDto: CreateUserDto): Promise<User> {
    return this.authService.register(createUserDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiResponse({
    status: 201,
    type: AuthLoginResponseDto,
    description: `Authorized`,
  })
  @ApiResponse({
    status: 401,
    description: `Unauthorized`,
  })
  @ApiBody({ type: AuthLoginDto })
  async login(@Request() req): Promise<AuthLoginResponseDto> {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('access-token')
  @UseInterceptors(ClassSerializerInterceptor)
  @Put('update/password')
  public async updatePassword(
    @Request() req,
    @Body()
    updatePasswordDto: AuthUpdatePasswordDto,
  ) {
    const updated = await this.updatePassword(req.user.id, updatePasswordDto);

    return {
      statusCode: 200,
      message: 'Password updated',
    };
  }
}
