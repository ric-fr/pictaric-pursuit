import {
  ClassSerializerInterceptor,
  Injectable,
  UnauthorizedException,
  UseInterceptors,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { AuthLoginResponseDto } from './dto/auth-login-response.dto';
import { AuthUpdatePasswordDto } from './dto/auth-update-password.dto';
import { JwtUserDto } from './dto/jwt-user.dto';
import { PasswordService } from '../users/password.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly passwordService: PasswordService,
    private readonly usersService: UsersService,
  ) {}

  async register(userDto: CreateUserDto): Promise<User> {
    return this.usersService.create(userDto);
  }

  async login(user: User): Promise<AuthLoginResponseDto> {
    const payload: JwtUserDto = { sub: user.id, username: user.name };

    const response: AuthLoginResponseDto = {
      token: this.jwtService.sign(payload),
    };

    return response;
  }

  async validateUser(login: string, password: string): Promise<User | null> {
    const user = await this.usersService.findByLogin(login);

    if (
      user &&
      user.password &&
      (await this.passwordService.validatePassword(password, user.password))
    ) {
      return user;
    }

    return null;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  async updatePassword(
    id: string,
    payload: AuthUpdatePasswordDto,
  ): Promise<User> {
    const user = await this.prisma.user.findUnique({
      where: { id },
    });

    // check old passwords
    const validateUser =
      user && (await this.validateUser(user.name, payload.password));

    if (!validateUser) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const password = await this.passwordService.hashPassword(
      payload.newPassword,
    );

    return this.prisma.user.update({
      where: { id },
      data: { password },
    });
  }
}

// export interface RegistrationStatus {
//   success: boolean;
//   message: string;
//   data?: User;
// }

// export interface RegistrationSeederStatus {
//   success: boolean;
//   message: string;
//   data?: User[];
// }
