import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, IsString } from 'class-validator';
import { User } from 'prisma/shared/classes/user';

export class ReqUserDto implements Partial<User> {
  @ApiProperty({ type: String, format: 'uuid' })
  @IsUUID(4)
  id: string;

  @ApiProperty({ type: String })
  @IsString()
  name: string;
}
