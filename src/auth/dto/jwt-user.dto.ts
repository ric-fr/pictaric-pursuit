export interface JwtUserDto {
  sub: string;
  username: string;
}
