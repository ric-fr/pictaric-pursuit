import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class AuthUpdatePasswordDto {
  @IsNotEmpty()
  @ApiProperty({ description: `Current user password` })
  password: string;

  @IsNotEmpty()
  @ApiProperty()
  newPassword: string;
}
