import { Logger, ValidationPipe } from '@nestjs/common';
import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { PrismaClientExceptionFilter, PrismaService } from 'nestjs-prisma';
import { AppModule } from './app.module';

const logger = new Logger('Main');
const port = Number(process.env.PORT) || 3000;
const swaggerPath = 'api';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: '*',
      credentials: true, // enables for GraphQl
    },
  });

  app.useGlobalPipes(
    new ValidationPipe({
      always: true,
      forbidNonWhitelisted: true,
      transform: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  );

  /*
  Issues with enableShutdownHooks#
  Prisma interferes with NestJS enableShutdownHooks. Prisma listens for shutdown signals and will call process.exit() before your application shutdown hooks fire.
  To deal with this, you would need to add a listener for Prisma beforeExit event.
  https://docs.nestjs.com/recipes/prisma
  */

  // enable shutdown hook
  const prismaService: PrismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);

  // prisma exception filter
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  const config = new DocumentBuilder()
    .setTitle(`PictaRIC Pursuit`)
    .setDescription(
      `Source <a href="https://gitlab.com/ric-fr/pictaric-pursuit" target="_blank">GitLab</a>`,
    )
    .setVersion(process.env.npm_package_version || 'nd')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'token',
    )
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'player-token',
    )
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(swaggerPath, app, document);

  await app.listen(port).finally(() => {
    logger.log(`Swagger http://localhost:${port}/${swaggerPath}`);
  });
}

bootstrap().finally(() => {
  logger.log(`App http://localhost:${port}`);
});
