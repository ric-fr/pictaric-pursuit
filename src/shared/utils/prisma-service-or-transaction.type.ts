import { PrismaService } from 'nestjs-prisma';

export type PrismaServiceOrTransaction =
  | PrismaService
  | Omit<
      PrismaService,
      '$connect' | '$disconnect' | '$on' | '$transaction' | '$use'
    >;
