import { defaultCardDeckDemocratie } from './default.cardDeck.Democratie';
import { defaultCardDeckEspritCritique } from './default.cardDeck.EspritCritique';
import { defaultCardDeckPersonnalitesSensibles } from './default.cardDeck.PersonnalitesSensibles';
import { defaultCardDeckSujetsSensibles } from './default.cardDeck.SujetsSensibles';

export const defaultCardDecks = [
  {
    group: 'A',
    label: 'top.cardSet.default.cardDeck.A',
    name: `Démocratie`,
    cards: defaultCardDeckDemocratie,
  },
  {
    group: 'B',
    label: 'top.cardSet.default.cardDeck.B',
    name: `Esprit Critique`,
    cards: defaultCardDeckEspritCritique,
  },
  {
    group: 'C',
    label: 'top.cardSet.default.cardDeck.C',
    name: `Sujets Sensibles`,
    cards: defaultCardDeckSujetsSensibles,
  },
  {
    group: 'D',
    label: 'top.cardSet.default.cardDeck.D',
    name: `Personnalités Sensibles`,
    cards: defaultCardDeckPersonnalitesSensibles,
  },
];
