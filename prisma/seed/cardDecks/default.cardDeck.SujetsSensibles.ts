export const defaultCardDeckSujetsSensibles = [
  'Politicien',
  'Assemblée nationale',
  'Sénat',
  'Élus',
  'Complotisme',
  'Gauche',
  'Droite',
  'Fascistes',
  'Gilets jaunes',
  'Antifa',
  'Télévision',
  'Langue de bois',
  'Promesse',
  'Union européenne',
  'Journalistes',
  'Mondialisme',
  'mondialisation',
  'Libre-échange',
  'Lobby',
  'Populisme',
  'Divertissement',
  'Sécurité ',
  'Le progrès',
  'Manifestation',
  'Grève',
  'Droit de l’homme',
  'GUILLOTINE',
  'Bureaucratie',
  'Fonctionnaire',
  'Représentant',
  'Corruption',
  'Union Européenne',
  'Traités internationaux',
  'Ennemi',
  'Journalisme',
  'Syndicats',
  'Austérité',
  'Racisme',
  'Vidéoprotection',
  'Bio',
  'Acquis sociaux',
  'Dette',
  'Immigration',
  'Guerre',
  'Précarisation',
  'Mafia',
  'Féminisme',
  'Frontières',
];
