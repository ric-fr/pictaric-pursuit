import { Edge } from './edge';
import { Game } from './game';
import { PlayerAction } from './player_action';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class Node {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiPropertyOptional({ type: String })
  type?: string;

  @ApiPropertyOptional({ type: String })
  label?: string;

  @ApiPropertyOptional({ type: String })
  name?: string;

  @ApiPropertyOptional({ type: String })
  json?: string;

  @ApiPropertyOptional({ type: String })
  content?: string;

  @ApiPropertyOptional({ type: String })
  description?: string;

  @ApiProperty({ isArray: true, type: () => Edge })
  ancestorEdges: Edge[];

  @ApiProperty({ isArray: true, type: () => Edge })
  descendantEdges: Edge[];

  @ApiProperty({ isArray: true, type: () => Game })
  boardGames: Game[];

  @ApiProperty({ isArray: true, type: () => Game })
  cardSetGames: Game[];

  @ApiProperty({ isArray: true, type: () => PlayerAction })
  playerMoves: PlayerAction[];
}
