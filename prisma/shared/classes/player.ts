import { Game } from './game';
import { PlayerAction } from './player_action';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class Player {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiPropertyOptional({ type: Date })
  acceptedAt?: Date;

  @ApiProperty({ type: String })
  name: string;

  @ApiProperty({ type: String })
  gameId: string;

  @ApiPropertyOptional({ type: () => Game })
  game?: Game;

  @ApiProperty({ isArray: true, type: () => PlayerAction })
  playerActions: PlayerAction[];
}
