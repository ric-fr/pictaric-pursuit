import { User as _User } from './user';
import { Game as _Game } from './game';
import { Player as _Player } from './player';
import { PlayerAction as _PlayerAction } from './player_action';
import { Node as _Node } from './node';
import { Edge as _Edge } from './edge';

export namespace PrismaModel {
  export class User extends _User {}
  export class Game extends _Game {}
  export class Player extends _Player {}
  export class PlayerAction extends _PlayerAction {}
  export class Node extends _Node {}
  export class Edge extends _Edge {}

  export const extraModels = [User, Game, Player, PlayerAction, Node, Edge];
}
