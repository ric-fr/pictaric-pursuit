import { Player } from './player';
import { Node } from './node';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class PlayerAction {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: String })
  json: string;

  @ApiProperty({ type: String })
  playerId: string;

  @ApiProperty({ type: () => Player })
  player: Player;

  @ApiPropertyOptional({ type: String })
  moveToNodeId?: string;

  @ApiPropertyOptional({ type: () => Node })
  moveTo?: Node;
}
