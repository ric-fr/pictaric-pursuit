import { Node } from './node';
import { Player } from './player';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class Game {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: Boolean })
  private: boolean;

  @ApiPropertyOptional({ type: String })
  name?: string;

  @ApiPropertyOptional({ type: String })
  secret?: string;

  @ApiProperty({ type: String })
  boardNodeLabel: string = 'top.board.default';

  @ApiProperty({ type: String })
  cardSetNodeLabel: string = 'top.cardSet.default';

  @ApiPropertyOptional({ type: Date })
  startedAt?: Date;

  @ApiPropertyOptional({ type: Date })
  endedAt?: Date;

  @ApiProperty({ type: Number })
  lock: number;

  @ApiProperty({ type: () => Node })
  board: Node;

  @ApiProperty({ type: () => Node })
  cardSet: Node;

  @ApiProperty({ isArray: true, type: () => Player })
  players: Player[];
}
