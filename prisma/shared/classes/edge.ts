import { Node } from './node';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class Edge {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: Date })
  createdAt: Date;

  @ApiProperty({ type: Date })
  updatedAt: Date;

  @ApiProperty({ type: String })
  ancestorNodeId: string;

  @ApiProperty({ type: String })
  descendantNodeId: string;

  @ApiPropertyOptional({ type: String })
  type?: string;

  @ApiPropertyOptional({ type: String })
  label?: string;

  @ApiPropertyOptional({ type: String })
  name?: string;

  @ApiPropertyOptional({ type: String })
  json?: string;

  @ApiPropertyOptional({ type: String })
  content?: string;

  @ApiPropertyOptional({ type: String })
  description?: string;

  @ApiProperty({ type: () => Node })
  ancestorNode: Node;

  @ApiProperty({ type: () => Node })
  descendantNode: Node;
}
