export enum ModelNamesUpper {
  User = "User",
  Game = "Game",
  Player = "Player",
  PlayerAction = "PlayerAction",
  Node = "Node",
  Edge = "Edge",
}

export enum ModelNamesLower {
  user = "user",
  game = "game",
  player = "player",
  playerAction = "playerAction",
  node = "node",
  edge = "edge",
}

export type TModelNames =
  | "User"
  | "Game"
  | "Player"
  | "PlayerAction"
  | "Node"
  | "Edge";
