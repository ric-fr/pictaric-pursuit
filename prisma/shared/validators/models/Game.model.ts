import { IsString, IsDefined, IsDate, IsBoolean, IsOptional, IsInt } from "class-validator";
import { Node, Player } from "./";

export class Game {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsDefined()
    @IsBoolean()
    private!: boolean;

    @IsOptional()
    @IsString()
    name?: string;

    @IsOptional()
    @IsString()
    secret?: string;

    @IsDefined()
    @IsString()
    boardNodeLabel!: string;

    @IsDefined()
    @IsString()
    cardSetNodeLabel!: string;

    @IsOptional()
    @IsDate()
    startedAt?: Date;

    @IsOptional()
    @IsDate()
    endedAt?: Date;

    @IsDefined()
    @IsInt()
    lock!: number;

    @IsDefined()
    board!: Node;

    @IsDefined()
    cardSet!: Node;

    @IsDefined()
    players!: Player[];
}
