import { IsString, IsDefined, IsDate, IsOptional } from "class-validator";
import { Game, PlayerAction } from "./";

export class Player {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsOptional()
    @IsDate()
    acceptedAt?: Date;

    @IsDefined()
    @IsString()
    name!: string;

    @IsDefined()
    @IsString()
    gameId!: string;

    @IsOptional()
    game?: Game;

    @IsDefined()
    playerActions!: PlayerAction[];
}
