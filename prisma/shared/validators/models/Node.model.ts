import { IsString, IsDefined, IsDate, IsOptional } from "class-validator";
import { Edge, Game, PlayerAction } from "./";

export class Node {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsOptional()
    @IsString()
    type?: string;

    @IsOptional()
    @IsString()
    label?: string;

    @IsOptional()
    @IsString()
    name?: string;

    @IsOptional()
    @IsString()
    json?: string;

    @IsOptional()
    @IsString()
    content?: string;

    @IsOptional()
    @IsString()
    description?: string;

    @IsDefined()
    ancestorEdges!: Edge[];

    @IsDefined()
    descendantEdges!: Edge[];

    @IsDefined()
    boardGames!: Game[];

    @IsDefined()
    cardSetGames!: Game[];

    @IsDefined()
    playerMoves!: PlayerAction[];
}
