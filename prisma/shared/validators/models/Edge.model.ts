import { IsString, IsDefined, IsDate, IsOptional } from "class-validator";
import { Node } from "./";

export class Edge {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsDefined()
    @IsString()
    ancestorNodeId!: string;

    @IsDefined()
    @IsString()
    descendantNodeId!: string;

    @IsOptional()
    @IsString()
    type?: string;

    @IsOptional()
    @IsString()
    label?: string;

    @IsOptional()
    @IsString()
    name?: string;

    @IsOptional()
    @IsString()
    json?: string;

    @IsOptional()
    @IsString()
    content?: string;

    @IsOptional()
    @IsString()
    description?: string;

    @IsDefined()
    ancestorNode!: Node;

    @IsDefined()
    descendantNode!: Node;
}
