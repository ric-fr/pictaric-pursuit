import { IsString, IsDefined, IsDate, IsOptional } from "class-validator";
import { Player, Node } from "./";

export class PlayerAction {
    @IsDefined()
    @IsString()
    id!: string;

    @IsDefined()
    @IsDate()
    createdAt!: Date;

    @IsDefined()
    @IsDate()
    updatedAt!: Date;

    @IsDefined()
    @IsString()
    json!: string;

    @IsDefined()
    @IsString()
    playerId!: string;

    @IsDefined()
    player!: Player;

    @IsOptional()
    @IsString()
    moveToNodeId?: string;

    @IsOptional()
    moveTo?: Node;
}
