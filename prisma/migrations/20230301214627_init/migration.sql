-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "activatedAt" DATETIME,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "deletedAt" DATETIME
);

-- CreateTable
CREATE TABLE "Game" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "private" BOOLEAN NOT NULL DEFAULT false,
    "name" TEXT,
    "secret" TEXT,
    "boardNodeLabel" TEXT NOT NULL DEFAULT 'top.board.default',
    "cardSetNodeLabel" TEXT NOT NULL DEFAULT 'top.cardSet.default',
    CONSTRAINT "Game_boardNodeLabel_fkey" FOREIGN KEY ("boardNodeLabel") REFERENCES "Node" ("label") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "Game_cardSetNodeLabel_fkey" FOREIGN KEY ("cardSetNodeLabel") REFERENCES "Node" ("label") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Player" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "name" TEXT NOT NULL,
    "gameId" TEXT NOT NULL,
    CONSTRAINT "Player_gameId_fkey" FOREIGN KEY ("gameId") REFERENCES "Game" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "PlayerAction" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "json" TEXT NOT NULL,
    "playerId" TEXT NOT NULL,
    "moveToNodeId" TEXT,
    CONSTRAINT "PlayerAction_playerId_fkey" FOREIGN KEY ("playerId") REFERENCES "Player" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "PlayerAction_moveToNodeId_fkey" FOREIGN KEY ("moveToNodeId") REFERENCES "Node" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Node" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "type" TEXT,
    "label" TEXT,
    "name" TEXT,
    "json" TEXT,
    "content" TEXT,
    "description" TEXT
);

-- CreateTable
CREATE TABLE "Edge" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "ancestorNodeId" TEXT NOT NULL,
    "descendantNodeId" TEXT NOT NULL,
    "type" TEXT,
    "label" TEXT,
    "name" TEXT,
    "json" TEXT,
    "content" TEXT,
    "description" TEXT,
    CONSTRAINT "Edge_ancestorNodeId_fkey" FOREIGN KEY ("ancestorNodeId") REFERENCES "Node" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "Edge_descendantNodeId_fkey" FOREIGN KEY ("descendantNodeId") REFERENCES "Node" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "User_name_key" ON "User"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Player_name_gameId_key" ON "Player"("name", "gameId");

-- CreateIndex
CREATE INDEX "Node_name_idx" ON "Node"("name");

-- CreateIndex
CREATE INDEX "Node_type_idx" ON "Node"("type");

-- CreateIndex
CREATE UNIQUE INDEX "Node_label_key" ON "Node"("label");

-- CreateIndex
CREATE INDEX "Edge_ancestorNodeId_descendantNodeId_idx" ON "Edge"("ancestorNodeId", "descendantNodeId");

-- CreateIndex
CREATE UNIQUE INDEX "Edge_label_key" ON "Edge"("label");
