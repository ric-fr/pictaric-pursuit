-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Game" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "private" BOOLEAN NOT NULL DEFAULT false,
    "name" TEXT,
    "secret" TEXT,
    "boardNodeLabel" TEXT NOT NULL DEFAULT 'top.board.default',
    "cardSetNodeLabel" TEXT NOT NULL DEFAULT 'top.cardSet.default',
    "startedAt" DATETIME,
    "endedAt" DATETIME,
    "lock" INTEGER NOT NULL DEFAULT 0,
    CONSTRAINT "Game_boardNodeLabel_fkey" FOREIGN KEY ("boardNodeLabel") REFERENCES "Node" ("label") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "Game_cardSetNodeLabel_fkey" FOREIGN KEY ("cardSetNodeLabel") REFERENCES "Node" ("label") ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "new_Game" ("boardNodeLabel", "cardSetNodeLabel", "createdAt", "endedAt", "id", "name", "private", "secret", "startedAt", "updatedAt") SELECT "boardNodeLabel", "cardSetNodeLabel", "createdAt", "endedAt", "id", "name", "private", "secret", "startedAt", "updatedAt" FROM "Game";
DROP TABLE "Game";
ALTER TABLE "new_Game" RENAME TO "Game";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
