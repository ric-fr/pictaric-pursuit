import { PrismaClient, Prisma, Node, Edge } from '@prisma/client';
import { defaultCardDeckDemocratie } from './seed/cardDecks/default.cardDeck.Democratie';
import { defaultCardDeckEspritCritique } from './seed/cardDecks/default.cardDeck.EspritCritique';
import { defaultCardDeckPersonnalitesSensibles } from './seed/cardDecks/default.cardDeck.PersonnalitesSensibles';
import { defaultCardDeckSujetsSensibles } from './seed/cardDecks/default.cardDeck.SujetsSensibles';
import { defaultCardDecks } from './seed/cardDecks/default.cardDecks';
// import { Edge } from './shared/classes/edge';
// import { Node } from './shared/classes/node';

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  log: ['warn', 'error'],
});

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

export async function main() {
  console.log(`Seeding cardSet default`);

  const edges: Edge[] = [];

  // prisma.deleteMany enumerates every ID of elements to delete resulting a in crash of the sqlLite connector
  // Using a raw query seems to work fine.
  // await prisma.node.deleteMany({
  //   where: {
  //     OR: [
  //       { label: { equals: 'top.cardSet.default' } },
  //       {
  //         label: {
  //           startsWith: 'top.cardSet.default.',
  //         },
  //       },
  //     ],
  //   },
  // });

  await prisma.$queryRaw`DELETE FROM "Node" WHERE label = 'top.cardSet.default' OR label LIKE 'top.cardSet.default.%'`;

  const cardSetDefault = await prisma.node.create({
    data: <Prisma.NodeCreateInput>{
      type: 'cardSet',
      label: 'top.cardSet.default',
      content: `Cartes PictaRIC`,
      name: `Cartes PictaRIC`,
      // descendantEdges: {
      //   create: {
      //     ancestorNode: {},
      //     // descendantNode: {
      //     //   create: {
      //     //     label: 'top.cardSet.default.1',
      //     //   },
      //     // },
      //   },
      //   // descendantEdges: {
      //   //   create: {

      //   //   }
      //   // }
      // },
    },
  });

  for await (const cardDeck of defaultCardDecks) {
    const { name, label, cards } = cardDeck;

    const subset = await nodeCreateDescendant(cardSetDefault, {
      label,
      type: 'cardDeck',
      content: name,
      name,
    });

    let i = 1;
    for await (const content of cards) {
      await nodeCreateDescendant(subset, {
        label: `${label}.${String(i++).padStart(4, '0')}`,
        type: 'card',
        content,
        name,
      });
    }
  }

  // await nodeCreateDescendant(cardSetDefault, {
  //   type: 'card',
  //   label: 'top.cardSet.default.subsetA.1',
  // });
  // await nodeCreateDescendant(cardSetDefault, {
  //   type: 'card',
  //   label: 'top.cardSet.default.subsetA.2',
  // });

  // const cardSetDefaultSetA1 = await prisma.node.create({
  //   data: {
  //     label: 'top.cardSet.default.subsetA.1',
  //   },
  // });

  // await prisma.edge.create({
  //   data: {
  //     ancestorNodeId: cardSetDefault.id,
  //     descendantNodeId: cardSetDefaultSetA1.id,
  //   },
  // });

  // const cardSetDefaultSetA2 = await prisma.node.create({
  //   data: {
  //     label: 'top.cardSet.default.subsetA.2',
  //   },
  // });

  const result = await prisma.node.findFirst({
    where: { label: 'top.cardSet.default' },
    select: {
      label: true,
      descendantEdges: {
        orderBy: {
          descendantNode: {
            label: 'asc',
          },
        },
        select: { descendantNode: { select: { label: true } } },
      },
    },
    // include: {
    //   descendantEdges: {
    //     orderBy: {
    //       descendantNode: {
    //         label: 'asc',
    //       },
    //     },
    //     select: {
    //       descendantNode: {
    //         select: { label: true },
    //       },
    //     },
    //     // include: {
    //     //   descendantNode: {
    //     //     select: {
    //     //       label: true,
    //     //     },
    //     //   },
    //     // },
    //   },
    // },
  });

  console.log('cardSet.default', JSON.stringify(result));

  /**
   * Seeding board default
   */

  console.log(`Seeding board default`);

  // await prisma.node.deleteMany({
  //   where: {
  //     OR: [
  //       { label: { equals: 'top.board.default' } },
  //       {
  //         label: {
  //           startsWith: 'top.board.default.',
  //         },
  //       },
  //     ],
  //   },
  // });

  await prisma.$queryRaw`DELETE FROM "Node" WHERE label = 'top.board.default' OR label LIKE 'top.board.default.%'`;

  const boardDefault = await prisma.node.create({
    data: {
      type: 'board',
      label: 'top.board.default',
      name: `Plateau simple`,
      content: `Course de chevaux`,
      // json: JSON.stringify({
      //   start: ['top.board.default.Start'],
      //   win: ['top.board.default.Win'],
      // }),
    },
  });

  const boardDefaultStart = await nodeCreateDescendant(boardDefault, {
    type: 'boardStart',
    label: 'top.board.default.Start',
    name: 'Départ',
    content: 'Départ',
  });

  const boardDefaultCompleted = await nodeCreateDescendant(boardDefault, {
    type: 'boardCompleted',
    label: 'top.board.default.Completed',
    name: 'Arrivée',
    content: 'Arrivée',
  });

  const boardPath: Node[] = [];
  for (let i = 1; i <= 16; i++) {
    const cardDeck = defaultCardDecks[(i - 1) % 4];

    boardPath.push(
      await nodeCreateDescendant(boardDefaultStart, {
        type: 'boardPath',
        label: `top.board.default.path.${String(i).padStart(4, '0')}`,
        name: `${i}`,
        content: cardDeck.group,
        json: JSON.stringify({
          cardDeck: cardDeck.label.replace(/^(.*)\./, ''), // Keep text after last dot
        }),
      }),
    );
  }

  // connect last node of circuit to Arrival
  // await nodeCreateDescendant(
  //   boardPath[boardPath.length - 1],
  //   boardDefaultCompleted,
  // );
  await prisma.edge.create({
    data: {
      ancestorNodeId: boardPath[boardPath.length - 1].id,
      descendantNodeId: boardDefaultCompleted.id,
    },
  });
}

export async function nodeCreateDescendant(
  nodeA: Node,
  nodeB: Prisma.NodeCreateInput,
  type: string | null = null,
): Promise<Node> {
  const data: Prisma.NodeCreateInput = {
    ...nodeB,
    ancestorEdges: {
      // connectOrCreate: {
      //   where: {

      //   }
      // }
      create: {
        // type,
        ancestorNode: {
          connect: {
            id: nodeA.id,
          },
        },
      },
    },
  };

  // const newCard = await prisma.node.upsert({
  //   where: {
  //     label: data.label,
  //   },
  //   update: {},
  //   create: data,
  // });

  const newCard = await prisma.node.create({
    data,
  });

  // const newEdge = await prisma.edge.create({
  //   data: {
  //     ancestorNodeId: nodeA.id,
  //     descendantNodeId: newCard.id,
  //   },
  // });

  return newCard;
}
