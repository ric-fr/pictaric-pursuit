export default json;

export enum GameActionEnum {
  AllPlayersPullDices = 'ALL_PLAYERS_PULL_DICES', // Aka server random

  Play = 'PLAY',
}

export const json = {
  rules: {
    playOrder: {
      action: GameActionEnum.AllPlayersPullDices,
      meta: {
        numberOfDices: 1,
        facets: 6,
        type: 'sum',
        winner: 'smallest',
        distinct: true,
      },
    },
    start: {
      actions: [{}],
    },
  },
};
